package ru.laskin.tree.service;

import ru.laskin.tree.model.GraphPath;
import ru.laskin.tree.model.Node;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

// В реализации используется отметка о посещении узла в сете settledNodes
public class LossCalculatorService {

    private Set<Long> settledNodes = new HashSet<>();
    private final static boolean debug = false;
    private List<GraphPath> paths = new ArrayList<>();

    private static void logging(String string) {
         if (debug) {
             System.out.println(string);
         }
    }

//    Метод рекурсивной обработки графа по алгоритму Depth-first search
    public List<GraphPath> calculate(Node origin, Node destination, Float weight, String path) {

        logging("I'm on " + origin.getId() + " with calculated weight " + weight +  " and path " + path);

        settledNodes.add(origin.getId());

        logging("Node have adj: ");
        for (Node node : origin.getAdjacentNodes().keySet()) {
            logging("\t" + node.getId() + " with weight " + origin.getAdjacentNodes().get(node));
        }

        for (Node node : origin.getAdjacentNodes().keySet()) {
//            logging("Go to " + node.getId() + " destination " + destination.getId());
            if (node.getId().equals(destination.getId())) {
                Float pathWeight = weight * origin.getAdjacentNodes().get(node);
                logging("We found path");
                path += " -> " + destination.getId();
                logging(path + " weight " + pathWeight.toString());
                paths.add(new GraphPath(path, pathWeight));
                break;
            } else if (!settledNodes.contains(node.getId())) {
                logging("Go to " + node.getId());
                calculate(node, destination, weight * origin.getAdjacentNodes().get(node), path.concat(" -> " + node.getId()));
            }
        }


        return paths;
    }

}
