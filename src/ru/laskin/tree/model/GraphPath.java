package ru.laskin.tree.model;

public class GraphPath {
    private String path;
    private Float weight;

    public GraphPath(String path, Float weight) {
        this.path = path;
        this.weight = weight;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "GraphPath{" +
                "path='" + path + '\'' +
                ", weight=" + weight +
                '}';
    }
}
