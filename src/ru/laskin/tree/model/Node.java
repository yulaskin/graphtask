package ru.laskin.tree.model;

import java.util.HashMap;
import java.util.Map;

public class Node {

    private Long id;
    private Map<Node, Float> adjacentNodes = new HashMap<>();

    public Node(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Map<Node, Float> getAdjacentNodes() {
        return adjacentNodes;
    }

    public void setAdjacentNodes(Map<Node, Float> adjacentNodes) {
        this.adjacentNodes = adjacentNodes;
    }

    public void addNode(Node node, Float cost) {
        this.adjacentNodes.put(node, cost);
    }

}
