package ru.laskin.tree.util;

import ru.laskin.tree.model.Node;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

// Класс для загрузки конфигурации сети из файлов или сокета
public class ConfgirationLoader {

    private static String NETWORK_CONFIG = "main/resources/treeconfig.csv";
    private static boolean debug = true;

    private static void logging(String string) {
        if (debug) {
            System.out.println(string);
        }
    }


    public Node getNodeOnGraph(HashMap<Long, Node> graph, Long nodeId) {
        Node node;
        if (graph.containsKey(nodeId)) {
            node = graph.get(nodeId);
        } else {
            node = new Node(nodeId);
            graph.put(nodeId,node);
        }
        return node;
    }

    public void loadNetwork(HashMap<Long, Node> graph) {
        ConfgirationLoader confgirationLoader = new ConfgirationLoader();
        ClassLoader classLoader = confgirationLoader.getClass().getClassLoader();
        File networkConfig = new File(classLoader.getResource(NETWORK_CONFIG).getFile());


        Node root = new Node(1L);
        graph.put(1L, root);


        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(networkConfig));
            String line = reader.readLine();

            while (line != null) {
                System.out.println(line);

                String[] configline = line.split(",");

                logging("Node parent:" + configline[0] + " child: " + configline[1] + " value " + configline[2]);

                long parentId = Long.parseLong(configline[0]);
                long childId = Long.parseLong(configline[1]);
                float weight = Long.parseLong(configline[2]) / 100F;

                Node node = this.getNodeOnGraph(graph, parentId);
                Node childNode = this.getNodeOnGraph(graph, childId);

                // связи будут 2-направленные
                node.addNode(childNode, weight);
                childNode.addNode(node, weight);


                line = reader.readLine();


            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
