package ru.laskin.tree;

// Any global declataions can be done here

import ru.laskin.tree.model.GraphPath;
import ru.laskin.tree.model.Node;
import ru.laskin.tree.service.LossCalculatorService;
import ru.laskin.tree.util.ConfgirationLoader;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.List;

class ClientProcessorApp extends Thread {

    Socket clientSocket;
    InputStream clientInputStream;
    OutputStream clientOutputStream;


    private static String TESTINPUT = "main/resources/input.csv";

    private static HashMap<Long, Node> graph = new HashMap<>();
    private static boolean debug = true;

    private static void logging(String string) {
        if (debug) {
            System.out.println(string);
        }
    }

    ClientProcessorApp(Socket socket) throws IOException {
        clientSocket = socket;
        clientInputStream = clientSocket.getInputStream();
        clientOutputStream = clientSocket.getOutputStream();
    }

    // Запускаем просто из main
    public static void main(String args[] ) throws Exception {

        ConfgirationLoader confgirationLoader = new ConfgirationLoader();
        confgirationLoader.loadNetwork(graph);

        System.out.println("Network configuration: ");

        for (Node value : graph.values()) {
            logging(value.getId().toString());
            for (Node node : value.getAdjacentNodes().keySet()) {
                logging("\t" + node.getId() + " with weight " + value.getAdjacentNodes().get(node));
            }
        }



        processClientInput();

    }

    // Метод обрабатывает тестовый ввод из файла, можно поменять на
    private static void processClientInput() throws IOException {

        System.out.println("### Starting count ###");
//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        ConfgirationLoader confgirationLoader = new ConfgirationLoader();
        ClassLoader classLoader = confgirationLoader.getClass().getClassLoader();
        File clientInputSample = new File(classLoader.getResource(TESTINPUT).getFile());

        BufferedReader br = new BufferedReader(new FileReader(clientInputSample));
        String line = br.readLine();

        while (line != null) {

            String[] clientInput = line.split(",");
            long init = Long.parseLong(clientInput[0]);
            long destination = Long.parseLong(clientInput[1]);
            float natural = Float.parseFloat(clientInput[2]);

            LossCalculatorService lossCalculatorService = new LossCalculatorService();
            logging("New calculation start: " + init + " End: " + destination);
            List<GraphPath> paths = lossCalculatorService.calculate(graph.get(init), graph.get(destination),1F , String.valueOf(init));


            float calculatedProbability = 0f;
            for (GraphPath path : paths) {
                logging(path.toString());
                if (path.getWeight() > calculatedProbability) {
                    calculatedProbability = path.getWeight();
                }
            }



//            System.out.println("Packet loss in task: " + Math.pow(10, natural));
            double threshold = Math.pow(10, natural);

            if (calculatedProbability > threshold) {
                write_string_to_stdout( "YES with probability " + calculatedProbability + " and threshold " + Math.pow(10, natural));
            } else {
                write_string_to_stdout( "NO with probability " + calculatedProbability + " and threshold " + Math.pow(10, natural));
            }

            line = br.readLine();
        }

    }




    /*
    This function is called only once before any client connection is accepted by the server.
    Read any global datasets or configurations here
    */
    public static void init_server() {
        System.out.println("Initializing server");
    }

    /*
    Write your code here
    This function is called everytime a new connection is accepted by the server
    */
    public void run() {
        do {
            String message = null;

            /* read message */
            try {
                message = read_string_from_socket();
            } catch (IOException ex) {
                System.out.println("Exception: " + ex);
            }

            /* End of operation on this client */
            if (message.equals("END"))
                break;

            System.out.println("Received = " + message);

            /* write message */
            try {
                write_string_to_socket(message);
            } catch (IOException ex) {
                System.out.println("Exception: " + ex);
            }
        } while(true);

        // Send end of communication. Very important!
        try {
            write_string_to_socket("END");
        } catch (IOException ex) {
            System.out.println("Exception: " + ex);
        }

        try {
            close_socket();
        } catch (IOException ex) {
            System.out.println("Exception: " + ex);
        }

        return;
    }

    /*
    This function encapsulates the communication protocol.
    Do not edit this function
    */
    public String read_string_from_socket() throws IOException {
        // Read payload
        byte[] len_network_order = new byte[4];
        clientInputStream.read(len_network_order);

        // Get message length from payload
        ByteBuffer bb = ByteBuffer.wrap(len_network_order);
        int message_length = bb.getInt();

        // Read message length bytes
        byte[] message_bytes = new byte[message_length];
        clientInputStream.read(message_bytes);

        // Convert bytes to string and return
        String message = new String(message_bytes, "UTF-8");
        return message;
    }

    /*
    This function encapsulates the communication protocol.
    Do not edit this function
    */
    public void write_string_to_socket(String message) throws IOException {
        // Read length of message and write message header
        int messageLength = message.length();
        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.order(ByteOrder.BIG_ENDIAN);
        bb.putInt(messageLength);
        bb.flip();
        clientOutputStream.write(bb.array());

        // Now write the message itself
        byte[] message_bytes = message.getBytes();
        bb = ByteBuffer.allocate(messageLength);
        bb.order(ByteOrder.BIG_ENDIAN);
        bb.put(message_bytes);
        bb.flip();
        clientOutputStream.write(bb.array());
    }

    /*
    This function encapsulates the communication protocol.
    Do not edit this function
    */
    public void close_socket() throws IOException {
        clientInputStream.close();
        clientOutputStream.close();
        clientSocket.close();
    }


    public static void write_string_to_stdout(String message) {
        System.out.println(message);
    }



}